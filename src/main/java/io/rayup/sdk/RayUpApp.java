package io.rayup.sdk;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.rayup.sdk.model.*;
import io.rayup.sdk.net.RayUpApiResponse;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;


public class RayUpApp {

    private String accessKeyId;
    private String accessSecretKey;

    private static final String RUS_API_URL_BASE = "https://api.rayup.io";

    private OkHttpClient httpClient;

    private Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    private static Logger logger = Logger.getLogger("RayUpApp");

    /**
     * Static initialize RayUp enviroment, and return RayUp instance.
     */
    public static RayUpApp initialize(String accessKeyId, String accessSecretKey) {
        RayUpApp app = new RayUpApp();
        app.accessKeyId = accessKeyId;
        app.accessSecretKey = accessSecretKey;
        app.httpClient = new OkHttpClient();
        return app;
    }

    /**
     * 分页加载 coin maps
     *
     * @param page  分页模式的页号，从 0 开始，默认为 0
     * @param countPerPage  分页加载的每页加载数目，默认为 10，每页最多 100
     *
     * @return 返回加载的 coin 列表
     */
    public List<Coin> loadCoinMaps(int page, int countPerPage) {

        if (page < 0 || countPerPage <= 0) {
            logger.warning("loadCoinMaps - invalid input params, page:" + page + ", countPerPage:" + countPerPage);
            return null;
        }

        final String path = "/v1/coin/map";
        Map<String, String> query = new HashMap<>();
        query.put("page", String.valueOf(page));
        query.put("count", String.valueOf(countPerPage));
        final String reqUrl = RUS_API_URL_BASE + path + "?page=" + page + "&count=" + countPerPage;
        Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("loadCoinMaps - can not generate headers");
            return null;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("loadCoinMaps - response: " + response);
            if (response.code() != 200) {
                logger.info("loadCoinMaps - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("loadCoinMaps - response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("coins")) {
                    List<Coin> coins = gson.fromJson(gson.toJson(apiResponse.data.get("coins")), new TypeToken<List<Coin>>(){}.getType());
                    logger.fine("loadCoinMaps - coins: " + coins);
                    return coins;
                }
            }
        } catch (IOException e) {
            logger.warning("loadCoinMaps - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("loadCoinMaps - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }

        return null;
    }

    /**
     * 获取指定的一个或多个 symbol 的 coin 信息
     *
     * @param symbols  可以一次指定多个 symbol
     *
     * @return  返回 coin 列表
     */
    public List<Coin> getCoinsBySymbols(String [] symbols) {

        if (null == symbols || symbols.length == 0) {
            return null;
        }

        final String paramSymbols = String.join(",", symbols);
        final String path = "/v1/coin/info";
        Map<String, String> query = new HashMap<>();
        query.put("symbol", paramSymbols);
        final String reqUrl = RUS_API_URL_BASE + path + "?symbol=" + paramSymbols;
        Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("getCoinsBySymbols - can not generate headers");
            return null;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getCoinsBySymbols - response: " + response);
            if (response.code() != 200) {
                logger.info("getCoinsBySymbols - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("getCoinsBySymbols - response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("coins")) {
                    List<Coin> coins = gson.fromJson(gson.toJson(apiResponse.data.get("coins")), new TypeToken<List<Coin>>(){}.getType());
                    logger.fine("getCoinsBySymbols - coins: " + coins);
                    return coins;
                }
            }

            return null;
        } catch (IOException e) {
            logger.warning("getCoinsBySymbols - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("getCoinsBySymbols - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }
    }

    /**
     * 获取指定法币的最新汇率
     *
     * @param leftSymbol  指定的 1 个单位的法币符号, 如: USD
     * @param rightSymbol  右侧需要对比的法币符号, 如: CNY
     *
     * @return 返回获取到的最新汇率, 如果不存在, 则返回 NULL
     */
    public CurrencyRate getLatestLegalCurrencyRate(String leftSymbol, String rightSymbol) {

        if (leftSymbol == null || leftSymbol.isEmpty()) {
            logger.info("getLatestLegalCurrencyRate - leftSymbol invalid.");
            return null;
        }

        if (rightSymbol == null || rightSymbol.isEmpty()) {
            logger.info("getLatestLegalCurrencyRate - rightSymbol invalid.");
            return null;
        }

        final String path = "/v1/currency/rates/latest";
        Map<String, String> query = new HashMap<>();
        query.put("left", leftSymbol);
        query.put("right", rightSymbol);
        final String reqUrl = RUS_API_URL_BASE + path + "?left=" + leftSymbol + "&right=" + rightSymbol;
        Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("getLatestLegalCurrencyRate - can not generate headers");
            return null;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getLatestLegalCurrencyRate - response: " + response);
            if (response.code() != 200) {
                logger.info("getLatestLegalCurrencyRate - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null) {
                    CurrencyRate currencyRate = gson.fromJson(gson.toJson(apiResponse.data), new TypeToken<CurrencyRate>(){}.getType());
                    logger.fine("currency rate: " + currencyRate);
                    return currencyRate;
                }
            }
        } catch (IOException e) {
            logger.warning("getLatestLegalCurrencyRate - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("getLatestLegalCurrencyRate - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }

        return null;
    }

    /**
     * 获取指定 coin 以指定法币计价的最新 quote 信息
     *
     * @param coinCode  coin code that RayUp defined.
     * @param currencySymbol  Currency symbol, eg: USD, CNY.
     *
     * @return Coin quote.
     */
    public CoinQuote getLatestCoinQuotesByCode(int coinCode, String currencySymbol) {

        if (coinCode <= 0) {
            logger.warning(String.format("Invalid <coinCode:%d>", coinCode));
            return null;
        }

        if (currencySymbol == null || currencySymbol.isEmpty()) {
            logger.warning("Invalid <currencySymbol>");
            return null;
        }

        final String path = "/v1/coin/quotes/latest";
        Map<String, String> query = new HashMap<>();
        query.put("code", String.valueOf(coinCode));
        query.put("convert", currencySymbol);
        final String reqUrl = RUS_API_URL_BASE + path + "?code=" + String.valueOf(coinCode) + "&convert=" + currencySymbol;
        Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("getLatestCoinQuotesByCode - can not generate headers");
            return null;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getLatestCoinQuotesByCode - response: " + response);
            if (response.code() != 200) {
                logger.info("getLatestCoinQuotesByCode - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("quote")) {
                    Map<String, Object> quote = (Map<String, Object>) apiResponse.data.get("quote");
                    if (quote.containsKey(currencySymbol)) {
                        Map<String, Object> currencySymbolQuote = (Map<String, Object>) quote.get(currencySymbol);
                        CoinQuote coinQuote = gson.fromJson(gson.toJson(currencySymbolQuote), new TypeToken<CoinQuote>(){}.getType());
                        coinQuote.setCoinCode(coinCode);
                        logger.fine("coin quote: " + coinQuote);
                        return coinQuote;
                    }
                }
            }
        } catch (IOException e) {
            logger.warning("getLatestCoinQuotesByCode - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("getLatestCoinQuotesByCode - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }

        return null;
    }

    /**
     * 获取指定一个或多个 coin 以指定的一种或多种法币计价的最新 quote 信息
     *
     * @param coinCodes  英文逗号分隔的 coin code 列表
     * @param currencySymbol  英文逗号分隔的 Currency symbol, eg: USD, CNY.
     *
     * @return Coin quotes.
     */
    public Map<Integer, Map<String, CoinQuote>> getLatestCoinQuotesByCodeV2(String coinCodes, String currencySymbol) {

        if (null == coinCodes || coinCodes.length() == 0) {
            logger.warning("Invalid <coinCode>");
            return null;
        }

        if (currencySymbol == null || currencySymbol.isEmpty()) {
            logger.warning("Invalid <currencySymbol>");
            return null;
        }

        final String path = "/v2/coin/quotes/latest";
        Map<String, String> query = new HashMap<>();
        query.put("code", coinCodes);
        query.put("convert", currencySymbol);
        final String reqUrl = RUS_API_URL_BASE + path + "?code=" + coinCodes + "&convert=" + currencySymbol;
        Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("getLatestCoinQuotesByCodeV2 - can not generate headers");
            return null;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getLatestCoinQuotesByCodeV2 - response: " + response);
            if (response.code() != 200) {
                logger.info("getLatestCoinQuotesByCodeV2 - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("quotes")) {
                    Map<Integer, Map<String, CoinQuote>> quotes = (Map<Integer, Map<String, CoinQuote>>) apiResponse.data.get("quotes");
                    logger.fine("coin quote: " + quotes);
                    return quotes;
                }
            }
        } catch (IOException e) {
            logger.warning("getLatestCoinQuotesByCodeV2 - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("getLatestCoinQuotesByCodeV2 - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }

        return null;
    }

    public static Map<String, Integer> coinSymbolMapCode = new HashMap<>();
    public static Map<Integer, String> coinCodeMapSymbol = new HashMap<>();
    static {
        coinSymbolMapCode.put("BTC", RayUpConst.COIN_BTC);
        coinSymbolMapCode.put("ETH", RayUpConst.COIN_ETH);
        coinSymbolMapCode.put("BRM", RayUpConst.COIN_BRM);
        coinSymbolMapCode.put("USDT", RayUpConst.COIN_USDT);

        coinCodeMapSymbol.put(RayUpConst.COIN_BTC, "BTC");
        coinCodeMapSymbol.put(RayUpConst.COIN_ETH, "ETH");
        coinCodeMapSymbol.put(RayUpConst.COIN_BRM, "BRM");
        coinCodeMapSymbol.put(RayUpConst.COIN_USDT, "USDT");
    }

    /**
     * 获取指定 coin 相对于一个或多个 base coin 的价格
     *
     * @param coinCode  交易对的左侧 coin 的 RayUp Id, 如 BRM 为 540, ETH 为 2 等等
     * @param converts  交易对的右侧基础 coin code, 英文逗号分隔, 支持的 base coin 有 BTC/ETH/USDT 等
     *
     * @return  返回一个或多个 coin pair price 记录
     */
    public List<CoinPairPrice> getLatestCoinPairPrice(int coinCode, String converts) {

        if (coinCode <= 0) {
            logger.warning(String.format("Invalid <coinCode:%d>", coinCode));
            return null;
        }

        if (converts == null || converts.isEmpty()) {
            logger.warning("Invalid <converts>");
            return null;
        }

        final String [] BASE_COINS = new String[] { "BTC", "ETH", "USDT" };
        String [] convertCoins = converts.split(",");
        String rightCoinCodes = "";
        for (String convertCoin: convertCoins) {
            boolean exists = false;
            for (String baseCoin: BASE_COINS) {
                if (convertCoin.equals(baseCoin)) {
                    exists = true;
                    rightCoinCodes += String.valueOf(coinSymbolMapCode.get(baseCoin)) + ",";
                }
            }

            if (!exists) {
                logger.warning("Unsupport <converts>");
                return null;
            }
        }

        // remove comma in tail
        rightCoinCodes = rightCoinCodes.substring(0, rightCoinCodes.length()-1);

        final String path = "/v1/coin/pairs/latest";
        Map<String, String> query = new HashMap<>();
        query.put("left", String.valueOf(coinCode));
        query.put("right", rightCoinCodes);
        final String reqUrl = RUS_API_URL_BASE + path + "?left=" + String.valueOf(coinCode) + "&right=" + rightCoinCodes;
        Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("getLatestCoinQuotesByCode - can not generate headers");
            return null;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getLatestCoinPairPrice - response: " + response);
            if (response.code() != 200) {
                logger.info("getLatestCoinPairPrice - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("pairs")) {
                    List<CoinPairPrice> coinPairPrices = gson.fromJson(gson.toJson(apiResponse.data.get("pairs")), new TypeToken<List<CoinPairPrice>>(){}.getType());
                    logger.fine("getLatestCoinPairPrice - coin pair prices: " + coinPairPrices);
                    return coinPairPrices;
                }
            }
        } catch (IOException e) {
            logger.warning("getLatestCoinPairPrice - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("getLatestCoinPairPrice - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }

        return null;
    }

    /*==================
     * Ethereum API
     *==================*/

    private EthBlock getEthBlockByHeightOrHash(String heightOrHash) {

        final String path = "/v1/eth/blocks/" + heightOrHash;
        Map<String, String> headers = generateRusApiHeaders("GET", path, null, null);
        if (headers == null) {
            logger.info("getEthBlockByHeightOrHash - can not generate headers");
            return null;
        }

        final String reqUrl = RUS_API_URL_BASE + path;
        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getEthBlockByHeightOrHash - response: " + response);
            if (response.code() != 200) {
                logger.info("getEthBlockByHeightOrHash - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("block")) {
                    EthBlock ethBlock = gson.fromJson(gson.toJson(apiResponse.data.get("block")), new TypeToken<EthBlock>(){}.getType());
                    logger.fine("getEthBlockByHeightOrHash - : " + ethBlock);
                    return ethBlock;
                }
            }

            return null;
        } catch (IOException e) {
            logger.warning("getEthBlockByHeightOrHash - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("getEthBlockByHeightOrHash - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }
    }

    /**
     * 获取指定区块高度的区块信息
     *
     * @param height  区块高度, 必须 > 0
     *
     * @return  如果不存在, 则返回 null; 否则返回 EthBlock 区块信息
     */
    public EthBlock getEthBlockByHeight(int height) {

        if (height <= 0) {
            logger.info("getEthBlockByHeight - invalid param <height> with " + height);
            return null;
        }

        return getEthBlockByHeightOrHash(String.valueOf(height));
    }

    /**
     * 获取指定区块 Hash 的区块信息
     *
     * @param blockHash  区块 Hash, 前缀以 0x 开头,并且长度必须为
     *
     * @return  如果不存在, 则返回 null; 否则返回 EthBlock 区块信息
     */
    public EthBlock getEthBlockByHash(String blockHash) {

        if (null == blockHash || blockHash.length() != RayUpConst.ETH_BLOCK_HASH_WITH_0x_LEN) {
            logger.info("getEthBlockByHash - invalid param <hash> with " + blockHash);
            return null;
        }

        return getEthBlockByHeightOrHash(blockHash);
    }

    /**
     * 获取 ERC20 token 总数
     *
     * @return >= 0 为总数, < 0 表示出现错误
     */
    public int getEthErc20TokenCount() {

        Map<String, String> query = new HashMap<>();
        query.put("type", "1");
        final String path = "/v1/eth/tokens/count";
        final String reqUrl = RUS_API_URL_BASE + path + "?type=1";
        final Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("getEthErc20TokenCount - can not generate headers");
            return -1;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getEthErc20TokenCount - response: " + response);
            if (response.code() != 200) {
                logger.info("getEthErc20TokenCount - status code: " + response.code());
                return -2;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("count")) {
                    int count = ((Double) apiResponse.data.get("count")).intValue();
                    logger.fine("getEthErc20TokenCount - : " + count);
                    return count;
                }
            }

            return 0;
        } catch (IOException e) {
            logger.warning("getEthErc20TokenCount - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return -3;
        } catch (NullPointerException e) {
            logger.warning("getEthErc20TokenCount - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return -4;
        }
    }

    /**
     * 分页加载 ERC20 tokens
     *
     * @param page  从 0 开始
     * @param count 每页加载的 token 数目
     *
     * @return  返回 Erc20 Token 列表
     */
    public List<EthToken> loadEthErc20Tokens(int page, int count) {

        if (page < 0 || count < 0) {
            logger.info("loadEthErc20Tokens - invalid param <page:" + page + ",count:" + count + ">");
            return null;
        }

        Map<String, String> query = new HashMap<>();
        query.put("type", "1");
        query.put("page", String.valueOf(page));
        query.put("count", String.valueOf(count));
        final String path = "/v1/eth/tokens";
        final String reqUrl = RUS_API_URL_BASE + path + "?type=1" + "&page=" + page + "&count=" + count;
        final Map<String, String> headers = generateRusApiHeaders("GET", path, query, null);
        if (headers == null) {
            logger.info("loadEthErc20Tokens - can not generate headers");
            return null;
        }

        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("loadEthErc20Tokens - response: " + response);
            if (response.code() != 200) {
                logger.info("loadEthErc20Tokens - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("tokens")) {
                    List<EthToken> tokens = gson.fromJson(gson.toJson(apiResponse.data.get("tokens")), new TypeToken<List<EthToken>>(){}.getType());
                    logger.fine("loadEthErc20Tokens - : " + tokens);
                    return tokens;
                }
            }

            return null;
        } catch (IOException e) {
            logger.warning("loadEthErc20Tokens - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("loadEthErc20Tokens - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }
    }

    /**
     * Get token information by contract address of token.
     *
     * @param tokenAddress  Contract address of token, support 0x or non 0x prefix.
     *
     * @return EthToken instance if exists, else null.
     */
    public EthToken getEthTokenByAddress(String tokenAddress) {

        final String path = "/v1/eth/tokens/" + tokenAddress;
        Map<String, String> headers = generateRusApiHeaders("GET", path, null, null);
        if (headers == null) {
            logger.info("getEthTokenByAddress - can not generate headers");
            return null;
        }

        final String reqUrl = RUS_API_URL_BASE + path;
        Request request = new Request.Builder()
                .url(reqUrl).headers(Headers.of(headers)).build();
        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("getEthTokenByAddress - response: " + response);
            if (response.code() != 200) {
                logger.info("getEthTokenByAddress - status code: " + response.code());
                return null;
            }

            if (response.body() != null) {
                String respBody = response.body().string();
                logger.info("response body: " + respBody);
                RayUpApiResponse apiResponse = gson.fromJson(respBody, new TypeToken<RayUpApiResponse>(){}.getType());
                if (apiResponse.status != null
                        && apiResponse.status.errorCode == 0
                        && apiResponse.data != null
                        && apiResponse.data.containsKey("token")) {
                    EthToken ethToken = gson.fromJson(gson.toJson(apiResponse.data.get("token")), new TypeToken<EthToken>(){}.getType());
                    logger.fine("getEthTokenByAddress - : " + ethToken);
                    return ethToken;
                }
            }

            return null;
        } catch (IOException e) {
            logger.warning("getEthTokenByAddress - execute [" + reqUrl + "] IOException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            logger.warning("getEthTokenByAddress - execute [" + reqUrl + "] NullPointerException: " + e.getMessage());
            return null;
        }
    }

    /*================================
     * RUS API Authorization Headers
     *===============================*/

    private Map<String, String> generateRusApiHeaders(String method, String path, Map<String, String> query, String data) {

        if (data == null) {
            data = "";
        }

        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            logger.info("Current java runtime library can not support MD5, " + e.getMessage());
            return null;
        }
        byte [] dataDigest = digest.digest(data.getBytes());
        String contentMD5 = Base64.getEncoder().encodeToString(dataDigest);

        String sigNonce = UUID.randomUUID().toString();
        String canonicalizedHeaders = "x-rus-signature-method:" + RayUpConst.RUS_SIG_METHOD + "\n"
                + "x-rus-signature-nonce:" + sigNonce + "\n"
                + "x-rus-signature-version:" + RayUpConst.RUS_SIG_VERSION;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String utcNow = sdf.format(new Date());

        String canonicalizedResource = path;
        if (query != null) {
            StringBuilder conditions = new StringBuilder();
            List<String> fieldNames = new ArrayList<>(query.keySet());
            if (fieldNames.size() > 0) {
                Collections.sort(fieldNames);
            }

            for (String key : fieldNames) {
                if (conditions.length() > 0) {
                    conditions.append("&");
                }
                conditions.append(key).append("=").append(query.get(key));
            }

            canonicalizedResource += '?' + conditions.toString();
        }

        String toSign = method + "\n"
                + contentMD5 + "\n"
                + utcNow + "\n"
                + canonicalizedHeaders + "\n"
                + canonicalizedResource;

        logger.fine("==> toSign: " + toSign);

        String messageSig;
        try {
            SecretKeySpec signingKey = new SecretKeySpec(accessSecretKey.getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            messageSig = Base64.getEncoder().encodeToString(mac.doFinal(toSign.getBytes()));
            logger.info("==> messageSig: " + messageSig);
        } catch (NoSuchAlgorithmException e) {
            logger.info("Current java runtime library can not support HmacSHA1, " + e.getMessage());
            return null;
        } catch (InvalidKeyException e) {
            logger.info("Sign exception, " + e.getMessage());
            return null;
        }

        Map<String, String> headers = new HashMap<>();
        headers.put("Date", utcNow);
        headers.put("Content-Type", RayUpConst.RUS_CONTENT_TYPE);
        headers.put("Content-MD5", contentMD5);
        headers.put("x-rus-signature-method", RayUpConst.RUS_SIG_METHOD);
        headers.put("x-rus-signature-version", RayUpConst.RUS_SIG_VERSION);
        headers.put("x-rus-signature-nonce", sigNonce);
        headers.put("Authorization", "rus " + accessKeyId + ":" + messageSig);
        logger.info("==> generated headers:" + headers);
        return headers;
    }

    @Override
    public String toString() {
        return "RayUpApp{" +
                "accessKeyId='" + accessKeyId + '\'' +
                ", accessSecretKey='" + accessSecretKey + '\'' +
                '}';
    }

    public static void main(String[] args ) {
        String accessKeyId = "727965f2c89511e8af75560001a43649";
        String accessKeySecret = "cnlo/siVEeivdVYAAaQ2SQ==";
        RayUpApp app = RayUpApp.initialize(accessKeyId, accessKeySecret);
        logger.info("==> app: " + app);

        //======= Coin & Currency =======

        // Coin map
        List<Coin> coins = app.loadCoinMaps(0, 10);
        logger.info("==> coins: " + coins);

        // coin info
        coins = app.getCoinsBySymbols(new String[] {"BRM", "ETH", "BTC"});
        logger.info("==> getCoinsBySymbols = coins: " + coins);

        // 获取 CNY/USD 的最新汇率
        CurrencyRate currencyRate = app.getLatestLegalCurrencyRate("USD", "CNY");
        logger.info("==> rate: " + currencyRate);

        // 获取 ETH 的最新 USD 行情
        CoinQuote coinQuote = app.getLatestCoinQuotesByCode(RayUpConst.COIN_ETH, "USD");
        logger.info("==> <ETH> quote: " + coinQuote);
        logger.info("==> quotes: " + app.getLatestCoinQuotesByCodeV2("2657,2", "USD,CNY"));

        // coin code = BRM(BrahmaOS) = 540
        coinQuote = app.getLatestCoinQuotesByCode(RayUpConst.COIN_BRM, "CNY");
        logger.info("==> <BRM> quote: " + coinQuote);

        // 获取 BRM 与 ETH/USDT 的汇率
        List<CoinPairPrice> coinPairPrices = app.getLatestCoinPairPrice(RayUpConst.COIN_BRM, "ETH,USDT");
        logger.info("==> <BRM> prices: " + coinPairPrices);

        //======= Ethereum =======

        final int blockHeight = 6824754;
        EthBlock ethBlock = app.getEthBlockByHeight(blockHeight);
        logger.info("==> eth block of height <" + blockHeight + "> : " + ethBlock);

        final String ethBlockHash = "0xd7a94f8e98c38dae2a4e96f8f55de01ee221ab86ce611dff52f2da02cf3c5ec8";
        ethBlock = app.getEthBlockByHash(ethBlockHash);
        logger.info("==> eth block of hash <" + ethBlockHash + "> : " + ethBlock);

        logger.info("==> token count: " + app.getEthErc20TokenCount());
        List<EthToken> tokens = app.loadEthErc20Tokens(0, 10);
        logger.info("==> tokens: " + tokens);

        EthToken ethToken = app.getEthTokenByAddress("0xd7732e3783b0047aa251928960063f863ad022d8");
        logger.info("==> token: " + ethToken);
    }
}
