package io.rayup.sdk;


/**
 * Define some constants.
 */
public class RayUpConst {

    public static final String RUS_SIG_METHOD = "HMAC-SHA1";
    public static final String RUS_SIG_VERSION = "1";
    public static final String RUS_CONTENT_TYPE = "application/json;charset=utf-8";

    public static final int ETH_BLOCK_HASH_LEN = 64;
    public static final int ETH_BLOCK_HASH_WITH_0x_LEN = 66;

    public static final int ETH_TX_HASH_LEN = 64;
    public static final int ETH_TX_HASH_WITH_0x_LEN = 66;

    public static final int COIN_BTC = 1;
    public static final int COIN_ETH = 1027;
    public static final int COIN_BRM = 2657;
    public static final int COIN_USDT = 825;
}
