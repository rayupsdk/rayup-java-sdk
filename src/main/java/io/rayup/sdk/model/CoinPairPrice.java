package io.rayup.sdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * 存储交易对价格
 * 交易对右侧为 Base Coin
 * 如: BRM/ETH(left = BRM, right = ETH)
 * 表示 1 个 BRM 可以兑换多少 ETH
 */
public class CoinPairPrice {

    @Expose
    @SerializedName("left")
    private int leftCoinCode;

    @Expose
    @SerializedName("right")
    private int rightCoinCode;

    @Expose
    @SerializedName("price")
    private float price;

    @Expose
    @SerializedName("last_updated")
    private Date lastUpdated;

    @Override
    public String toString() {
        return "CoinPairPrice{" +
                "leftCoinCode=" + leftCoinCode +
                ", rightCoinCode=" + rightCoinCode +
                ", price=" + price +
                ", lastUpdated=" + lastUpdated +
                '}';
    }

    public int getLeftCoinCode() {
        return leftCoinCode;
    }

    public int getRightCoinCode() {
        return rightCoinCode;
    }

    public float getPrice() {
        return price;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }
}
