package io.rayup.sdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Ethereum Token
 */
public class EthToken {

    @Expose
    @SerializedName("code")
    private int coinCode;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("symbol")
    private String symbol;

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("logo")
    private String logo;

    @Expose
    @SerializedName("last_updated")
    private Date lastUpdated;

    public int getCoinCode() {
        return coinCode;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getAddress() {
        return address;
    }

    public String getLogo() {
        return logo;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public String toString() {
        return "EthToken{" +
                "coinCode=" + coinCode +
                ", name='" + name + '\'' +
                ", symbol='" + symbol + '\'' +
                ", address='" + address + '\'' +
                ", logo='" + logo + '\'' +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
