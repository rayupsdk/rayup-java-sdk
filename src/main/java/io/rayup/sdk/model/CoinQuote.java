package io.rayup.sdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;


/**
 * coin market quote
 */
public class CoinQuote {

    @Expose
    @SerializedName("code")
    private int coinCode;

    @Expose
    @SerializedName("exchange_code")
    private int exchangeCode;

    @Expose
    @SerializedName("price")
    private Float price;

    @Expose
    @SerializedName("market_cap")
    private Float marketCap;

    @Expose
    @SerializedName("volume_24h")
    private Float volume24h;

    @Expose
    @SerializedName("last_updated")
    private Date lastUpdated;

    @Expose
    @SerializedName("percent_change_1h")
    private Float change1h;

    @Expose
    @SerializedName("percent_change_24h")
    private Float change24h;

    @Expose
    @SerializedName("percent_change_7d")
    private Float change7d;

    public int getCoinCode() {
        return coinCode;
    }

    public void setCoinCode(int coinCode) {
        this.coinCode = coinCode;
    }

    public int getExchangeCode() {
        return exchangeCode;
    }

    public Float getPrice() {
        return price;
    }

    public Float getMarketCap() {
        return marketCap;
    }

    public Float getVolume24h() {
        return volume24h;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public Float getChange1h() {
        return change1h;
    }

    public Float getChange24h() {
        return change24h;
    }

    public Float getChange7d() {
        return change7d;
    }

    @Override
    public String toString() {
        return "CoinQuote{" +
                "exchangeCode=" + exchangeCode +
                ", price=" + price +
                ", marketCap=" + marketCap +
                ", volume24h=" + volume24h +
                ", lastUpdated=" + lastUpdated +
                ", change1h=" + change1h +
                ", change24h=" + change24h +
                ", change7d=" + change7d +
                '}';
    }
}
