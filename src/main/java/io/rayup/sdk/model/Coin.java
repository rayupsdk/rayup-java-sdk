package io.rayup.sdk.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.Map;

public class Coin {

    @Expose
    @SerializedName("code")
    private int code;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("symbol")
    private String symbol;

    @Expose
    @SerializedName("token")
    private Map<String, Object> token;

    @Expose
    @SerializedName("last_updated")
    private Date lastUpdated;

    @Expose
    @SerializedName("logo")
    private String logo;

    @Override
    public String toString() {
        return "Coin{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", symbol='" + symbol + '\'' +
                ", token=" + token +
                ", lastUpdated=" + lastUpdated +
                ", logo='" + logo + '\'' +
                '}';
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public Map<String, Object> getToken() {
        return token;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public String getLogo() {
        return logo;
    }
}
