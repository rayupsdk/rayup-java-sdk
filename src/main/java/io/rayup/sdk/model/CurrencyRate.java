package io.rayup.sdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Legal currency rate.
 */
public class CurrencyRate {

    @Expose
    @SerializedName("left")
    private String leftCurrencySymbol;

    @Expose
    @SerializedName("right")
    private String rightCurrencySymbol;

    @Expose
    @SerializedName("rate")
    private float rate;

    @Expose
    @SerializedName("last_updated")
    private Date lastUpdated;

    @Override
    public String toString() {
        return "CurrencyRate{" +
                "leftCurrencySymbol='" + leftCurrencySymbol + '\'' +
                ", rightCurrencySymbol='" + rightCurrencySymbol + '\'' +
                ", rate=" + rate +
                ", lastUpdated=" + lastUpdated +
                '}';
    }

    public String getLeftCurrencySymbol() {
        return leftCurrencySymbol;
    }

    public String getRightCurrencySymbol() {
        return rightCurrencySymbol;
    }

    public float getRate() {
        return rate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }
}
