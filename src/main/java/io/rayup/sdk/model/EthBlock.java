package io.rayup.sdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Ethereum 区块信息
 */
public class EthBlock {

    @Expose
    @SerializedName("height")
    public int height;

    @Expose
    @SerializedName("hash")
    public String hash;

    @Expose
    @SerializedName("parent_hash")
    public String parentHash;

    @Expose
    @SerializedName("miner")
    public String miner;

    @Expose
    @SerializedName("tx_count")
    public int txCount;

    @Expose
    @SerializedName("time")
    public long time;

    @Override
    public String toString() {
        return "EthBlock{" +
                "height=" + height +
                ", hash='" + hash + '\'' +
                ", parentHash='" + parentHash + '\'' +
                ", miner='" + miner + '\'' +
                ", txCount=" + txCount +
                ", time=" + time +
                '}';
    }
}
