package io.rayup.sdk.net;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RayUpApiResponseStatus {

    @Expose
    @SerializedName("error_code")
    public int errorCode;

    @Expose
    @SerializedName("error_message")
    public String errorMessage;

    @Expose
    @SerializedName("credit_count")
    public int creditCount;

    @Override
    public String toString() {
        return "Status{" +
                "errorCode=" + errorCode +
                ", errorMessage=" + errorMessage +
                ", creditCount=" + creditCount +
                '}';
    }
}
