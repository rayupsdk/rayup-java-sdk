package io.rayup.sdk.net;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * RayUp api response, include:
 *
 *  - status
 *  - data
 */
public class RayUpApiResponse {

    @Expose
    @SerializedName("data")
    public Map<String, Object> data;

    @Expose
    @SerializedName("status")
    public RayUpApiResponseStatus status;

    @Override
    public String toString() {
        return "RayUpApiResponse{" +
                "data=" + data +
                ", status=" + status +
                '}';
    }
}
