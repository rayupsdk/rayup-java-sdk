Change Log
==========

## Version 1.3.0

* Add some Ethereum blockchain apis, including:
  * **getEthBlockByHeight**
  * **getEthBlockByHash**

* Add latest coin quotes api with multiple coin codes:
  * **getLatestCoinQuotesByCodeV2** 

## Version 1.2.0

* Change **Coin** data member *name* from *Map* to *String*: only return the specific language coin name.
* Update some field serialize name of model **CoinQuote**.
* Change **CoinPairPrice** data member left and right code with code, not support symbol that should already cached in local app.
