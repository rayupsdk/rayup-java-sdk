
## 概述

RayUpJavaSdk 为开发者提供了访问 RayUp Service(RUS) 服务的 Java SDK, 该库中封装了:

- RUS Authentication 的业务逻辑, 开发者只需要调用 RayUpApp.initialize() 方法, 在初始化之后, 后续的调用由 RayUpJavaSdk 对请求进行安全性封装;

## 环境及依赖库

- Java 1.8+

- gson

```
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>2.8.5</version>
</dependency>
```

- okhttp3

```
<dependency>
    <groupId>com.squareup.okhttp3</groupId>
    <artifactId>okhttp</artifactId>
    <version>3.11.0</version>
</dependency>
```

## 编译构建

```shell
$ mvn clean package
```

## 使用流程和示例

- 初始化

使用 RayUp Java SDK 之前，首先需要到 [RayUp 官网](https://www.rayu.io) 注册申请 RUS API KEY，包括 **accessKeyId** 和 **accessKeySecret**。

之后在导入 RayUpJavaSdk 至您的 Java/Android 项目之后，执行如下初始化 RUS SDK 代码：

```Java
String accessKeyId = "727965f2c89511e8af7556xyz1a43649";
String accessKeySecret = "cnlo/siVEeivdVYAAaQxyz==";
RayUpApp app = RayUpApp.initialize(accessKeyId, accessKeySecret);
```

后续可以使用 **RayUpApp** 实例来访问 RUS 的服务，支持的服务如下：

- **加载 coin map**

在使用之前建议加载 coin 信息，其中包括 RayUp 为 coin 分配的 code，后续的很多 API 都是基于该 coinCode。

如下为分页加载 CoinMap 列表：

```java
List<Coin> coins = app.loadCoinMaps(0, 10);
```

- **获取指定不同法币间的最新汇率**

```java
CurrencyRate currencyRate = app.getLatestLegalCurrencyRate("USD", "CNY");
```

- **获取指定 coin 与指定法币之间的最新汇率**

```java
CoinQuote coinQuote = app.getLatestCoinQuotesByCode(2, "USD");
```

- **获取指定的一个或多个 coin 与指定的一个或多个法币的币价信息**

```java
Map<Integer, Map<String, CoinQuote>> quotes = app.getLatestCoinQuotesByCodeV2("2,2657", "USD,CNY");
```

- **获取指定 coin 与指定 coin 之间的最新汇率**

如下示例获取 BRM/ETH 和 BRM/USDT 的交易对最新汇率：

```java
List<CoinPairPrice> coinPairPrices = app.getLatestCoinPairPrice(540, "ETH,USDT");
```

在 **CoinPairPrice** 中存储如下信息：

```java
public class CoinPairPrice {

    @Expose
    @SerializedName("left")
    private String leftSymbol;

    @Expose
    @SerializedName("right")
    private String rightSymbol;

    @Expose
    @SerializedName("price")
    private float price;

    @Expose
    @SerializedName("last_updated")
    private Date lastUpdated;
}
```